# IMPORTANT :

Une correction officielle est/va être publiée, je vous y renvoie

# TD Analyse L1

Ce dépôt est dédié à accueillir les corrections des TDs de Math104 (analyse) pour vous permettre de continuer de travailler pendant la suspension des cours.
Vous pouvez l'utiliser de trois manières :
- Vous avez fait un exercice, et vous voulez vérifier vos réponses
- Vous bloquez sur une question/un exercice, et vous voulez un exemple. Essayez alors de le refaire sans aide après quelques jours, pour vérifier que vous avez compris
- Vous voulez essayer LaTeX : dans ce cas, aidez moi à faire ces corrections :) Je pense notamment que je vais, dans un premier temps, me concentrer sur les exercices obligatoires, mais n'hésitez pas à faire les autres !

### Vérification des réponses :

Vous pouvez vérifier vos réponses à l'aide du logiciel `sagemath`. Exemple, pour déterminer le DL en 0, à l'ordre 4, selon la variable x, de ln(1+x) :
```python
>>> var('x')
>>> taylor(ln(1+x),x,0,4)
```
