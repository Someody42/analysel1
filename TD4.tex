\documentclass[a4paper]{article}
\setlength{\parindent}{0 pt}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{graphicx}
\usepackage[french]{babel}
\usepackage{amsmath}
\usepackage{array}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{stmaryrd}
\usepackage{tikz,tkz-tab}
\usepackage{systeme}
\usepackage{geometry}
\usepackage{enumerate}
\geometry{hmargin=2.5cm,vmargin=1.5cm}
\newcommand{\deriv}{\mathrm{d}}
\newcommand{\Id}{\mathrm{Id}}
\newcommand\fundef[3]{#1: \left\{\begin{array}{lc}#2\\#3\end{array}\right.}
\newcommand{\tq}{\text{ t.q }}
\newcommand{\ssi}{si et seulement si }
\newcommand{\e}{\mathrm{e}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\norm}[1]{\left\Vert #1\right\Vert}
\newcommand{\abs}[1]{\left\vert#1\right\vert}

\newtheoremstyle{exostyle}% name of the style to be used
  {\topsep}% measure of space to leave above the theorem. E.g.: 3pt
  {\topsep}% measure of space to leave below the theorem. E.g.: 3pt
  {}% name of font to use in the body of the theorem
  {0pt}% measure of space to indent
  {\bfseries}% name of head font
  {---}% punctuation between head and body
  { }% space after theorem head; " " = normal interword space
  {\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
\theoremstyle{exostyle}
\newtheorem{exo}{Exercice}
\renewcommand{\qed}{QED}
\numberwithin{equation}{exo}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\solset}{Sol}

\begin{document}
\title{Maths 104 : Correction du TD 4}
\author{Anatole DEDECKER}

\maketitle

On abrègera \og développement limité \fg{} en DL. \\
De plus, $\epsilon$ et $\epsilon_i$ (pour $i\in\N$) désignent des fonctions de limite 0 en 0.

\section{Calcul de DL en 0}

\exo{Sommes et produits (vérifié)}

\begin{enumerate}
  \item En utilisant les DL connus et la formule de la somme, on obtient :
  \begin{equation*}
    \forall x\in\R\quad\frac{1}{1-x}-\e^x=\sum_{k=0}^4 x^k - \sum_{k=0}^4 \frac{x^k}{k!} + x^4\epsilon(x)=\sum_{k=0}^4 \left(1-\frac{1}{k!}\right)x^k + x^4\epsilon(x)
  \end{equation*}
  \item Utilisons d'abord la formule du produit pour déterminer le DL à l'ordre 4 de $x\mapsto \frac{x}{2}\sin(x)$. On trouve :
  \begin{equation*}
    \forall x\in\R\quad \frac{x}{2}\sin(x) = \frac{x^2}{2} - \frac{x^4}{12} + x^4\epsilon_0(x)
  \end{equation*}
  En utilisant la formule de la somme, on trouve donc :
  \begin{equation*}
    \forall x\in\R\quad \cos(x)-1+\frac{x}{2}\sin(x) = -\frac{1}{24}x^4 + x^4\epsilon(x)
  \end{equation*}
  \item Remarquons que la fonction $x\mapsto\sqrt{1+x^2}$ est la composée de la fonction $x\mapsto x^2$, qui vaut bien 0 en 0, par la fonction $t\mapsto\left(1+t\right)^\frac{1}{2}$. On a alors :
  \begin{align*}
    \forall x\in\R\quad \sqrt{1+x^2}&=1+\frac{1}{2}x^2+\frac{\frac{1}{2}\left(\frac{1}{2}-1\right)}{2}x^4 + x^4\epsilon_0(x)\\
                                    &=1+\frac{1}{2}x^2-\frac{1}{8}x^4 + x^4\epsilon_0(x)
  \end{align*}
  \textbf{ATTENTION} : Lorsqu'on cherche le DL en un point $x\in\R$ d'une composée $u\circ v$, il faut utiliser le \underline{DL de $u$ en $v(x)$}. En particulier, comme nous n'étudions pour le moment que les DL en $0$, vous devez \textbf{absolument} vérifier qu'on a $v(0)=0$. \\\\

  Il reste donc à appliquer la formule du produit de DL, et on obtient :
  \begin{align*}
    \forall x\in\R\quad (x+1)\sqrt{1+x^2} &= 1+\frac{1}{2}x^2-\frac{1}{8}x^4 + x + \frac{1}{2}x^3 + x^4\epsilon(x)\\
                      &= 1+x+\frac{1}{2}x^2+\frac{1}{2}x^3-\frac{1}{8}x^4 + x^4\epsilon(x)
  \end{align*}
  \item \textbf{Méthode 1} (classique) :
  En utilisant la formule du produit, on trouve :
  \begin{align*}
    \forall x\in\R\quad \sin(x)\cos(x) &= x - \frac{1}{2}x^3 - \frac{1}{6}x^3 + x^4\epsilon(x)\\
                                        &= x - \frac{2}{3}x^3 + x^4\epsilon(x)
  \end{align*}
  \textbf{Méthode 2} (plus efficace) : on remarque $\forall x\in\R, \sin(x)\cos(x)=\frac{1}{2}\sin(2x)$. Comme $x\mapsto2x$ vaut bien 0 en 0, on trouve :
  \begin{align*}
    \forall x\in\R\quad \sin(x)\cos(x) &= \frac{1}{2}\left((2x) - \frac{1}{6}(2x)^3 + x^4\epsilon_0(x)\right)\\
                                       &= x - \frac{2}{3}x^3 + x^4\epsilon(x)
  \end{align*}
  \item En utilisant la formule du produit, on trouve :
  \begin{align*}
    \forall x\in\R\quad \e^x\sqrt{1+x} &= \e^x(1+x)^\frac{1}{2} \\
                                    &= 1 + \frac{1}{2}x + \frac{\frac{1}{2}\left(\frac{1}{2}-1\right)}{2}x^2 + \frac{\frac{1}{2}\left(\frac{1}{2}-1\right)\left(\frac{1}{2}-2\right)}{6}x^3 +
                                    \frac{\frac{1}{2}\left(\frac{1}{2}-1\right)\left(\frac{1}{2}-2\right)\left(\frac{1}{2}-3\right)}{24}x^4\\
                                    &\quad + x + \frac{1}{2}x^2 + \frac{\frac{1}{2}\left(\frac{1}{2}-1\right)}{2}x^3 + \frac{\frac{1}{2}\left(\frac{1}{2}-1\right)\left(\frac{1}{2}-2\right)}{6}x^4
                                    + \frac{1}{2}x^2 + \frac{1}{4}x^3 + \frac{\frac{1}{2}\left(\frac{1}{2}-1\right)}{4}x^4\\
                                    &\quad + \frac{1}{6}x^3 + \frac{1}{12}x^4 + \frac{1}{24}x^4\\
                                    &= 1 + \frac{3}{2} x + \frac{7}{8}x^2 + \frac{17}{48}x^3 + \frac{11}{128}x^4
  \end{align*}
\end{enumerate}

\exo{Quotient et composition (à faire)}

\exo{Intégration (vérifié)}

\begin{enumerate}
  \item On a $\forall x\in\R, \frac{1}{1+x} = (1+x)^{-1} = 1 - x + \frac{1}{2}x^2 - \frac{1}{3}x^3 + x^3\epsilon_0(x)$. En prenant la primitive, on trouve le DL à l'ordre 4 cherché :
  \begin{equation*}
    \forall x\in]-1,+\infty[\quad \ln(1+x) = ln(1) + x - \frac{1}{2}x^2 + \frac{1}{6}x^3 - \frac{1}{24}x^4 + x^4\epsilon(x)
  \end{equation*}
  \item La fontion $x\mapsto-x^2$ admet un DL de tout ordre en 0, et vaut 0 en 0. On trouve donc :
  \begin{align*}
    \forall x\in]-1,1[\quad \frac{1}{\sqrt{1-x^2}} &= (1-x^2)^{-\frac{1}{2}}\\
                                                    &= 1-\frac{1}{2}\left(-x^2\right) + x^3\epsilon_0(x)\\
                                                    &= 1+\frac{1}{2}x^2+x^3\epsilon_0(x)
  \end{align*}
  En prenant la primitive, on trouve le DL à l'ordre 4 cherché :
  \begin{equation*}
    \forall x\in]-1,1[\quad \arcsin(x) = \arcsin(0)+x+\frac{1}{6}x^3+x^4\epsilon(x)
  \end{equation*}
  \item La fontion $x\mapsto-x^2$ admet un DL de tout ordre en 0, et vaut 0 en 0. On trouve donc :
  \begin{align*}
    \forall x\in\R\quad \frac{1}{1+x^2} &= \frac{1}{1-\left(-x^2\right)}\\
                                        &= 1 + \left(-x^2\right) + x^3\epsilon_0(x)\\
                                        &= 1 -x^2+ x^3\epsilon_0(x)
  \end{align*}
  En prenant la primitive, on trouve le DL à l'ordre 4 cherché :
  \begin{equation*}
    \forall x\in\R\quad \arctan(x) = \arctan(0)+x-x^3+x^4\epsilon(x)
  \end{equation*}
  \item La fontion $x\mapsto x^2$ admet un DL de tout ordre en 0, et vaut 0 en 0. On trouve donc :
  \begin{equation*}
    \forall x\in\R\quad \e^{x^2} = 1 + x^2 + x^3\epsilon_0(x)
  \end{equation*}
  En intégrant, on trouve le DL à l'odre 4 cherché :
  \begin{align*}
    \forall x\in\R\quad \int_0^x \e^{t^2} \deriv t &= \int_0^0 \e^{t^2}\deriv t + x +\frac{1}{3}x^3 + x^4\epsilon(x)\\
                                                    &= x +\frac{1}{3}x^3 + x^4\epsilon(x)
  \end{align*}
\end{enumerate}

\end{document}
